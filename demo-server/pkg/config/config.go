package config

import (
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/jinzhu/configor"
)

const (
	EnvProduction = "PRODUCTION"
	EnvPrefix     = "DEMO"
)

type Config struct {
	Environment string           `yaml:"environment" env:"DEMO_ENVIRONMENT" default:"development"`
	ServiceName string           `yaml:"service_name" env:"DEMO_SERVICE_NAME" default:"demo-server"`
	Server      ServerConfig     `yaml:"server"         env:"-"`
	Mongo       MongoConfig      `yaml:"mongo" env:"-"`
	Migrations  MigrationsConfig `yaml:"migrations" env:"-"`
}

type ServerConfig struct {
	HttpServerPort int `yaml:"http_server_port" env:"DEMO_HTTP_SERVER_PORT" default:"8080"`
}

type MongoConfig struct {
	Database string `yaml:"database"          env:"DEMO_MONGO_DATABASE" default:"demo"`
	Uri      string `yaml:"uri"               env:"DEMO_MONGO_URI"`
}

type MigrationsConfig struct {
	URI     string `yaml:"uri"     env:"DEMO_MONGO_MIGRATION_URI"`
	Path    string `yaml:"path"    env:"DEMO_MIGRATIONS_PATH"`
	Enabled bool   `yaml:"enabled" env:"DEMO_MIGRATIONS_ENABLED"   default:"false"`
}

func NewConfig(path string) (*Config, error) {
	var cfg Config
	loader := configor.New(&configor.Config{ENVPrefix: EnvPrefix, ErrorOnUnmatchedKeys: true})
	if err := loader.Load(&cfg, path); err != nil {
		return nil, fmt.Errorf("failed to load config: %w", err)
	}

	if cfg.Environment != EnvProduction {
		spew.Dump(cfg)
	}

	return &cfg, nil
}
