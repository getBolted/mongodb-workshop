package encoder_test

import (
	"demo-server/pkg/encoder"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestEncode(t *testing.T) {
	r := require.New(t)
	testCases := []struct {
		longUrl  string
		shortUrl string
		error    error
	}{
		{
			longUrl:  "",
			shortUrl: "",
			error:    encoder.ErrBagLongURL,
		},
		{
			longUrl:  "http://localhost:8082/db/demo/",
			shortUrl: "KyIXJ2Vp5zZ",
			error:    nil,
		},
		{
			longUrl:  "https://google.com/maps",
			shortUrl: "IuLlr5p1Jux",
			error:    nil,
		},
		{
			longUrl:  "https://habr.com/ru/articles/773304/",
			shortUrl: "G5f24GlkWsx",
			error:    nil,
		},
		{
			longUrl:  "https://github.com/mongodb/mongo-go-driver/blob/v1/event/monitoring.go#L97",
			shortUrl: "7HqbM3eKxlW",
			error:    nil,
		},
	}

	for _, tc := range testCases {
		long, err := encoder.Encode(tc.longUrl)
		r.EqualValues(tc.error, err)
		r.EqualValues(tc.shortUrl, long)
	}
}
