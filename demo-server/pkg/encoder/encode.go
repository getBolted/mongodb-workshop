package encoder

import (
	"errors"
	"github.com/deatil/go-encoding/base62"
	"github.com/twmb/murmur3"
)

var ErrBagLongURL = errors.New("bad param - empty long_url")

func Encode(longURL string) (shortUrl string, err error) {
	if longURL == "" {
		return "", ErrBagLongURL
	}

	hash := murmur3.New64()
	hash.Write([]byte(longURL))
	shortUrl = base62.StdEncoding.EncodeToString(hash.Sum(nil))

	return shortUrl, nil
}
