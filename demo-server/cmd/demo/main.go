package main

import (
	"context"
	"demo-server/internal"
	"demo-server/pkg/config"
	"demo-server/pkg/shutdown"
	"log"
	"log/slog"
	"os"
	"time"
)

const (
	sleepTimeout = 3 * time.Second
)

func main() {
	cfg, err := config.NewConfig("./config.yaml")
	if err != nil {
		log.Printf("failed to load config.yaml: %v", err)
	}

	textHandler := slog.NewTextHandler(os.Stdout, nil)
	log := slog.New(textHandler)

	app := internal.NewApplication(log, cfg)
	shutdown.InitShutdown(log, app)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	shutdown.GC().Monitor(cancel)

	err = app.Run(ctx)
	if err != nil {
		log.With(slog.String("err_descr", err.Error())).Error("application Run() failed")
		shutdown.GC().Halt()
	} else {
		log.Info("server started")
	}

	shutdown.GC().Wait()
}
