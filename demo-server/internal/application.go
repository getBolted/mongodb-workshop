package internal

import (
	"context"
	"demo-server/internal/adapter/db"
	"demo-server/internal/adapter/rest"
	"demo-server/internal/core/services"
	"demo-server/internal/ports"
	"demo-server/pkg/config"
	"demo-server/pkg/migration"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log/slog"
	"sync"
	"time"
)

const (
	terminateTimeout = 2 * time.Minute
)

type Application struct {
	log         *slog.Logger
	cfg         *config.Config
	restServer  *rest.Server
	mongoClient *mongo.Client
}

type PublicContainer struct {
	ports.Stats
	ports.Url
}

func NewApplication(log *slog.Logger, conf *config.Config) *Application {
	return &Application{log: log, cfg: conf}
}

func (a *Application) Run(ctx context.Context) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	a.log.Info("Connecting to mongo...")
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(a.cfg.Mongo.Uri))
	if err != nil {
		return fmt.Errorf("new mongo client create error: %w", err)
	}

	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		return fmt.Errorf("new mongo primary node connect error: %w", err)
	}

	a.mongoClient = client
	database := client.Database(a.cfg.Mongo.Database)

	if a.cfg.Migrations.Enabled {
		migrationSvc := migration.NewMigrationsService(a.log, database)
		err = migrationSvc.RunMigrations(a.cfg.Migrations.Path)
		if err != nil {
			return fmt.Errorf("run migrations failed")
		}
	}

	urlRepo, err := db.CreateURLRepo(a.log, database)
	if err != nil {
		return fmt.Errorf("url repo create failed: %w", err)
	}
	statsRepo, err := db.CreateStatsRepo(a.log, database)
	if err != nil {
		return fmt.Errorf("url repo create failed: %w", err)
	}

	urlService := services.NewURLSvcImpl(a.log, statsRepo, urlRepo)
	statsService := services.NewStatsSvcImpl(a.log, statsRepo)

	pc := &PublicContainer{
		statsService,
		urlService,
	}

	a.restServer = rest.NewServer(a.log, a.cfg, pc)
	a.restServer.SetUp()

	go func() {
		if err = a.restServer.Start(); err != nil {
			a.log.Error(fmt.Sprintf("HTTP Server startup failed: %s", err.Error()))
		}
	}()

	return err
}

func (a *Application) Stop() {
	ctx, cancel := context.WithTimeout(context.Background(), terminateTimeout)
	defer cancel()

	wg := &sync.WaitGroup{}
	if a.mongoClient != nil {
		wg.Add(1)
		go func(ctx context.Context, wg *sync.WaitGroup) {
			defer wg.Done()
			a.log.Debug("Shutting down DB")
			if err := a.mongoClient.Disconnect(ctx); err != nil {
				a.log.Error("Failed to shut down DB in a proper way")
			}
		}(ctx, wg)
	}

	if a.restServer != nil {
		wg.Add(1)
		go func(ctx context.Context, wg *sync.WaitGroup) {
			defer wg.Done()
			a.log.Debug("Shutting down HTTP restServer")
			if err := a.restServer.Stop(ctx); err != nil {
				a.log.Error("Failed to shut down HTTP Server in a proper way")
			}
		}(ctx, wg)
	} else {
		wg.Done()
	}
}
