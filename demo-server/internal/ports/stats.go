//go:generate mockgen -source=stats.go -destination=mocks/stats_mock.go
package ports

import (
	"context"
	"demo-server/internal/core/model"
)

type Stats interface {
	GetStatisticsByShortURL(ctx context.Context, shortURL string) (model.ShortURLStatistics, error)
	GetStatisticsRecap(ctx context.Context) ([]model.ShortURLStatistics, error)
}
