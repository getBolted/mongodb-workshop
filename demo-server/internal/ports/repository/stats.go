//go:generate mockgen -source=stats.go -destination=mocks/stats_mock.go
package repository

import (
	"context"
	"demo-server/internal/core/model"
)

type StatsRepo interface {
	SaveStat(ctx context.Context, data model.RequestData) error
	GetStatByShortURL(ctx context.Context, shortURL string) (model.ShortURLStatistics, error)
	GetStatRecap(ctx context.Context) ([]model.ShortURLStatistics, error)
}
