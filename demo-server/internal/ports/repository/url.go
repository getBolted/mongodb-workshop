//go:generate mockgen -source=url.go -destination=mocks/url_mock.go
package repository

import (
	"context"
	"demo-server/internal/core/model"
)

type URLRepo interface {
	AddNewPair(ctx context.Context, data model.URLData) error
	GetByShortURL(ctx context.Context, shortURL string) (model.URLData, error)
	DeleteByShortURL(ctx context.Context, shortURL string) error
}
