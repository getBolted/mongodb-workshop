package ports

type Public interface {
	Stats
	Url
}
