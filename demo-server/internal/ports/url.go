//go:generate mockgen -source=url.go -destination=mocks/url_mock.go
package ports

import (
	"context"
	"demo-server/internal/core/model"
)

type Url interface {
	RequestOriginalURL(ctx context.Context, data model.RequestData) (string, error)
	GenerateShortURL(ctx context.Context, longURL string, ttl *int) (string, error)
	DropRecordByShortURL(ctx context.Context, shortURL string) error
}
