package response

import (
	openapiPublic "demo-server/api/openapi/public"
	"encoding/json"
	"log/slog"
	"net/http"
	"strings"
)

var (
	ErrBadRequest = openapiPublic.OutErrorMessage{
		Code:    http.StatusBadRequest,
		Message: "BAD_REQUEST_PARAMS",
	}
	ErrInternal = openapiPublic.OutErrorMessage{
		Code:    http.StatusInternalServerError,
		Message: "SERVER_SIDE_ERROR",
	}
	ErrTTLExpired = openapiPublic.OutErrorMessage{
		Code:    http.StatusPreconditionFailed,
		Message: "TTL_EXPIRED",
	}
	ErrNotFound = openapiPublic.OutErrorMessage{
		Code:    http.StatusNotFound,
		Message: "DATA_NOT_FOUND",
	}
)

type Response struct {
	logger *slog.Logger
}

func NewResponse(
	log *slog.Logger,
) *Response {
	return &Response{
		logger: log,
	}
}

func (s *Response) WJsonResponse(response interface{}, w http.ResponseWriter, code int) {
	var jsonData []byte

	if response != nil && response != http.NoBody {
		w.Header().Set("Accept", "application/json")

		var err error
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		jsonData, err = json.Marshal(response)
		if err != nil {
			s.logger.Error(err.Error())
			w.WriteHeader(http.StatusInternalServerError)

			return
		}
	}

	s.wResponse(jsonData, w, code)
}

func (s *Response) WErrorResponse(err error, w http.ResponseWriter) {
	switch {
	case strings.Contains(err.Error(), "validation"), strings.Contains(err.Error(), "bad param"):
		s.WJsonResponse(ErrBadRequest, w, ErrBadRequest.Code)
	case strings.Contains(err.Error(), "expired"):
		s.WJsonResponse(ErrTTLExpired, w, ErrTTLExpired.Code)
	case strings.Contains(err.Error(), "not found"):
		s.WJsonResponse(ErrNotFound, w, ErrNotFound.Code)
	default:
		s.WJsonResponse(ErrInternal, w, ErrInternal.Code)
	}
}

func (s *Response) wTypedErrorResponse(typedErr openapiPublic.OutErrorMessage, w http.ResponseWriter) {
	s.WJsonResponse(typedErr, w, typedErr.Code)
}

//nolint:errorlint
func (s *Response) ErrorHandlerFunc(w http.ResponseWriter, r *http.Request, err error) {
	s.logger.Error(err.Error())
	switch err.(type) {
	case *openapiPublic.RequiredParamError,
		*openapiPublic.RequiredHeaderError,
		*openapiPublic.InvalidParamFormatError,
		*openapiPublic.TooManyValuesForParamError,
		*openapiPublic.UnescapedCookieParamError:
		s.wTypedErrorResponse(ErrBadRequest, w)
	default:
		s.wTypedErrorResponse(ErrInternal, w)
	}
}

func (s *Response) wResponse(resp []byte, w http.ResponseWriter, code int) {
	w.WriteHeader(code)

	if len(resp) > 0 {
		_, err := w.Write(resp)
		if err != nil {
			s.logger.Error(err.Error())
		}
	}
}
