package handler

import (
	openapiPublic "demo-server/api/openapi/public"
	"demo-server/internal/core/model"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"io"
	"net/http"
	"time"
)

func (p PublicAPIImpl) URLShorten(w http.ResponseWriter, r *http.Request) {
	var err error
	ctx := r.Context()

	data, err := io.ReadAll(r.Body)
	if err != nil {
		err = fmt.Errorf("bad param - unable to read body: %w", err)
		p.logger.With(err).Error("http.URLShorten.ReadAll")
		p.WErrorResponse(err, w)

		return
	}

	defer func() {
		if err = r.Body.Close(); err != nil {
			p.logger.With(err).Error("http.URLShorten.Close")
		}
	}()

	req := &openapiPublic.InLongURL{}
	if err = json.Unmarshal(data, req); err != nil {
		err = fmt.Errorf("bad param - unable to parse body: %w", err)
		p.logger.With(err).Error("http.URLShorten.Unmarshal")
		p.WErrorResponse(err, w)

		return
	}

	if req.LongUrl == nil || *req.LongUrl == "" {
		err = fmt.Errorf("bad param - emtpy long_url: %w", err)
		p.logger.With(err).Error("http.URLShorten.checkLongURL")
		p.WErrorResponse(err, w)

		return
	}

	if req.Ttl != nil && *req.Ttl < 1 {
		err = fmt.Errorf("bad param - broken ttl: %w", err)
		p.logger.With(err).Error("http.URLShorten.checkTTL")
		p.WErrorResponse(err, w)

		return
	}

	shortURL, err := p.serviceContainer.GenerateShortURL(ctx, *req.LongUrl, req.Ttl)
	if err != nil {
		p.logger.With(err).Error("http.URLShorten.GenerateShortURL")
		p.WErrorResponse(err, w)

		return
	}

	resp := openapiPublic.OutShortURL{ShortUrl: &shortURL}
	p.WJsonResponse(resp, w, http.StatusOK)
}

func (p PublicAPIImpl) URLDrop(w http.ResponseWriter, r *http.Request, shortURL string) {
	var err error
	ctx := r.Context()

	if shortURL == "" {
		err = errors.New("validation error: short_url param is empty")
		p.logger.With(err).Error("handler.URLDrop.checkShortURL")
		p.WErrorResponse(err, w)

		return
	}

	err = p.serviceContainer.DropRecordByShortURL(ctx, shortURL)
	if err != nil {
		p.logger.With(err).Error("handler.URLDrop.DropRecordByShortURL")
		p.WErrorResponse(err, w)

		return
	}

	p.WJsonResponse(http.NoBody, w, http.StatusNoContent)
}

func (p PublicAPIImpl) URLGet(w http.ResponseWriter, r *http.Request, shortURL string) {
	var err error
	ctx := r.Context()

	if shortURL == "" {
		err = errors.New("validation error: short_url param is empty")
		p.logger.With(err).Error("handler.URLGet.checkShortURL")
		p.WErrorResponse(err, w)

		return
	}

	longURL, err := p.serviceContainer.RequestOriginalURL(ctx, model.RequestData{
		ShortURL:    shortURL,
		RequestedAt: time.Now(),
		UserAgent:   r.Header.Get("User-Agent"),
		Ip:          r.RemoteAddr,
	})

	if err != nil {
		p.logger.With(err).Error("handler.URLGet.RequestOriginalURL")
		p.WErrorResponse(err, w)

		return
	}

	http.Redirect(w, r, longURL, http.StatusFound)
}
