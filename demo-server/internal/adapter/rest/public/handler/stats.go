package handler

import (
	openapiPublic "demo-server/api/openapi/public"
	"errors"
	"net/http"
)

func (p PublicAPIImpl) GetStatsRecap(w http.ResponseWriter, r *http.Request) {
	var err error
	ctx := r.Context()

	stats, err := p.serviceContainer.GetStatisticsRecap(ctx)
	if err != nil {
		p.logger.With(err).Error("handler.GetStatsRecap.GetStatisticsRecap")
		p.WErrorResponse(err, w)

		return
	}

	recapStatistics := make([]openapiPublic.OutURLStatistics, 0, len(stats))
	for i := 0; i < len(stats); i++ {
		recapStatistics = append(recapStatistics, openapiPublic.OutURLStatistics{
			HitNumber:        &stats[i].HitNumber,
			LongUrl:          &stats[i].LongURL,
			UniqueIps:        &stats[i].UniqueIpAddrs,
			UniqueUserAgents: &stats[i].UniqueUserAgents,
		})
	}

	resp := openapiPublic.OutRecapStatistics{
		RecapStatistics: &recapStatistics,
	}

	p.WJsonResponse(resp, w, http.StatusOK)
}

func (p PublicAPIImpl) GetURLStats(w http.ResponseWriter, r *http.Request, shortURL string) {
	var err error
	ctx := r.Context()

	if shortURL == "" {
		err = errors.New("validation error: short_url param is empty")
		p.logger.With(err).Error("handler.GetURLStats.checkShortURL")
		p.WErrorResponse(err, w)

		return
	}

	stats, err := p.serviceContainer.GetStatisticsByShortURL(ctx, shortURL)
	if err != nil {
		p.logger.With(err).Error("handler.GetURLStats.GetStatisticsByShortURL")
		p.WErrorResponse(err, w)

		return
	}

	resp := openapiPublic.OutURLStatistics{
		HitNumber:        &stats.HitNumber,
		LongUrl:          &stats.LongURL,
		UniqueIps:        &stats.UniqueIpAddrs,
		UniqueUserAgents: &stats.UniqueUserAgents,
	}

	p.WJsonResponse(resp, w, http.StatusOK)
}
