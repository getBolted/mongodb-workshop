package handler

import (
	openapiPublic "demo-server/api/openapi/public"
	"demo-server/internal/adapter/rest/public/response"
	"demo-server/internal/ports"
	"log/slog"
)

var (
	_ openapiPublic.ServerInterface = &PublicAPIImpl{}
)

type PublicAPIImpl struct {
	serviceContainer ports.Public
	logger           *slog.Logger
	*response.Response
}

func NewAPIImpl(
	logger *slog.Logger,
	serviceRegistry ports.Public,
	resp *response.Response,
) openapiPublic.ServerInterface {
	return &PublicAPIImpl{
		serviceContainer: serviceRegistry,
		logger:           logger,
		Response:         resp,
	}
}
