package rest

import (
	"context"
	openapiPublic "demo-server/api/openapi/public"
	"demo-server/internal/adapter/rest/public/handler"
	"demo-server/internal/adapter/rest/public/response"
	"demo-server/internal/ports"
	"demo-server/pkg/config"
	"fmt"
	"github.com/go-chi/chi/v5"
	"log/slog"
	"net/http"
	"time"
)

const (
	readTimeout       = 1 * time.Second
	writeTimeout      = 1 * time.Second
	idleTimeout       = 30 * time.Second
	readHeaderTimeout = 2 * time.Second
)

type Server struct {
	httpServer      *http.Server
	config          *config.Config
	publicContainer ports.Public
	logger          *slog.Logger
	router          chi.Router
}

func NewServer(
	logger *slog.Logger,
	cfg *config.Config,
	publicContainer ports.Public,
) *Server {
	server := &Server{
		logger:          logger,
		config:          cfg,
		publicContainer: publicContainer,
	}

	return server
}

func (s *Server) SetUp() {
	s.router = chi.NewRouter()
	responser := response.NewResponse(s.logger)

	publicApi := handler.NewAPIImpl(s.logger, s.publicContainer, responser)
	publicRouter := openapiPublic.HandlerWithOptions(publicApi, openapiPublic.ChiServerOptions{
		ErrorHandlerFunc: responser.ErrorHandlerFunc,
	})
	s.router.Use()
	s.router.Mount("/", publicRouter)

	s.httpServer = &http.Server{
		ReadTimeout:       readTimeout,
		WriteTimeout:      writeTimeout,
		IdleTimeout:       idleTimeout,
		ReadHeaderTimeout: readHeaderTimeout,
		Addr:              fmt.Sprintf("0.0.0.0:%d", s.config.Server.HttpServerPort),
		Handler:           s.router,
	}
}

func (s *Server) Start() error {
	s.logger.Info(fmt.Sprintf("http server starts on port: %d", s.config.Server.HttpServerPort))
	return s.httpServer.ListenAndServe()
}

func (s *Server) Stop(ctx context.Context) error {
	s.logger.Info(fmt.Sprintf("http server starts on port: %d", s.config.Server.HttpServerPort))
	return s.httpServer.Shutdown(ctx)
}
