package db

import (
	"context"
	"demo-server/internal/core/model"
	"demo-server/internal/ports/repository"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"log/slog"
	"time"
)

const (
	statsCollection = "stats"
)

var _ repository.StatsRepo = (*StatsRepositoryImpl)(nil)

type StatsRepositoryImpl struct {
	logger *slog.Logger
	db     *mongo.Database
}

func (s StatsRepositoryImpl) SaveStat(ctx context.Context, data model.RequestData) error {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()

	var err error

	_, err = s.db.Collection(statsCollection).InsertOne(ctx, data)
	if err != nil {
		return fmt.Errorf("db.SaveStat.InsertOne:%w", err)
	}

	return nil
}

func (s StatsRepositoryImpl) GetStatByShortURL(ctx context.Context, shortURL string) (model.ShortURLStatistics, error) {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()

	var stats []model.ShortURLStatistics

	matchStage := bson.D{{"$match", bson.D{{"short_url", shortURL}}}}
	groupStage := bson.D{
		{"$group", bson.D{
			{"_id", "$long_url"},
			{"user_agents", bson.D{{"$addToSet", "$user_agent"}}},
			{"ips", bson.D{{"$addToSet", "$ip"}}},
			{"hit_num", bson.D{{"$sum", 1}}},
		}}}

	cursor, err := s.db.Collection(statsCollection).Aggregate(ctx, mongo.Pipeline{matchStage, groupStage})
	if err != nil {
		return model.ShortURLStatistics{}, fmt.Errorf("db.GetStatByShortURL.Aggregate:%w", err)
	}

	if err = cursor.All(ctx, &stats); err != nil {
		return model.ShortURLStatistics{}, fmt.Errorf("db.GetStatByShortURL.All:%w", err)
	}

	if len(stats) > 1 {
		return model.ShortURLStatistics{}, fmt.Errorf("db.GetStatByShortURL.checkResultsLen: there are more than one result")
	}

	return stats[0], nil
}

func (s StatsRepositoryImpl) GetStatRecap(ctx context.Context) ([]model.ShortURLStatistics, error) {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()

	var stats []model.ShortURLStatistics

	groupStage := bson.D{
		{"$group", bson.D{
			{"_id", "$long_url"},
			{"user_agents", bson.D{{"$addToSet", "$user_agent"}}},
			{"ips", bson.D{{"$addToSet", "$ip"}}},
			{"hit_num", bson.D{{"$sum", 1}}},
		}}}

	cursor, err := s.db.Collection(statsCollection).Aggregate(ctx, mongo.Pipeline{groupStage})
	if err != nil {
		return stats, fmt.Errorf("db.GetStatByShortURL.Aggregate:%w", err)
	}

	if err = cursor.All(ctx, &stats); err != nil {
		return stats, fmt.Errorf("db.GetStatByShortURL.All:%w", err)
	}

	return stats, nil
}

func CreateStatsRepo(logger *slog.Logger, db *mongo.Database) (*StatsRepositoryImpl, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	if db != nil {
		err := db.CreateCollection(ctx, statsCollection)
		if err != nil {
			var we mongo.CommandError
			if errors.As(err, &we) {
				if we.Code == collectionCreatedCode {
					err = nil
				}
			}

			if err != nil {
				return nil, err
			}
		}
	}

	return &StatsRepositoryImpl{
		logger: logger,
		db:     db,
	}, nil
}
