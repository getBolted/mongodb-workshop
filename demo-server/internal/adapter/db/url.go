package db

import (
	"context"
	"demo-server/internal/core/model"
	"demo-server/internal/ports/repository"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"log/slog"
	"time"
)

const (
	timeoutQuery          = time.Second * 10
	collectionCreatedCode = 48
	urlCollection         = "url"
)

var _ repository.URLRepo = (*URLRepositoryImpl)(nil)

type URLRepositoryImpl struct {
	logger *slog.Logger
	db     *mongo.Database
}

func (U URLRepositoryImpl) AddNewPair(ctx context.Context, data model.URLData) error {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()

	var err error

	_, err = U.db.Collection(urlCollection).InsertOne(ctx, data)
	if err != nil {
		return fmt.Errorf("db.AddNewPair.InsertOne:%w", err)
	}

	return nil
}

func (U URLRepositoryImpl) GetByShortURL(ctx context.Context, shortURL string) (model.URLData, error) {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()

	var err error

	filter := bson.M{
		"short_url": shortURL,
		"deleted_at": bson.M{
			"$eq": nil,
		},
	}

	response := model.URLData{}

	err = U.db.Collection(urlCollection).FindOne(ctx, filter).Decode(&response)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return response, fmt.Errorf("db.GetByShortURL.Decode:not found %w", err)
		}

		return response, fmt.Errorf("db.GetByShortURL.Decode:%w", err)
	}

	return response, nil
}

func (U URLRepositoryImpl) DeleteByShortURL(ctx context.Context, shortURL string) error {
	ctx, cancel := context.WithTimeout(ctx, timeoutQuery)
	defer cancel()

	var err error

	filter := bson.M{
		"short_url": shortURL,
	}

	update := bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "deleted_at", Value: time.Now()},
		}},
	}

	_, err = U.db.Collection(urlCollection).UpdateOne(ctx, filter, update)
	if err != nil {
		return fmt.Errorf("db.DeleteByShortURL.Decode::%w", err)
	}

	return nil
}

func CreateURLRepo(logger *slog.Logger, db *mongo.Database) (*URLRepositoryImpl, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	if db != nil {
		err := db.CreateCollection(ctx, urlCollection)
		if err != nil {
			var we mongo.CommandError
			if errors.As(err, &we) {
				if we.Code == collectionCreatedCode {
					err = nil
				}
			}

			if err != nil {
				return nil, err
			}
		}
	}

	return &URLRepositoryImpl{
		logger: logger,
		db:     db,
	}, nil
}
