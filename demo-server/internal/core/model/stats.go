package model

import "time"

type ShortURLStatistics struct {
	LongURL          string   `bson:"_id"`
	HitNumber        int64    `bson:"hit_num"`
	UniqueUserAgents []string `bson:"user_agents"`
	UniqueIpAddrs    []string `bson:"ips"`
}

type RequestData struct {
	RequestedAt time.Time `bson:"requested_at"`
	ShortURL    string    `bson:"short_url"`
	LongURL     string    `bson:"long_url"`
	UserAgent   string    `bson:"user_agent"`
	Ip          string    `bson:"ip"`
}
