package model

import "time"

type URLData struct {
	ShortURL  string     `bson:"short_url"`
	LongURL   string     `bson:"long_url"`
	ValidTill time.Time  `bson:"valid_till"`
	CreatedAt time.Time  `bson:"created_at"`
	DeletedAt *time.Time `bson:"deleted_at"`
}
