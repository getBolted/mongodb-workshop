package services

import (
	"context"
	"demo-server/internal/core/model"
	"demo-server/internal/ports"
	"demo-server/internal/ports/repository"
	"log/slog"
)

var _ ports.Stats = (*StatsSvcImpl)(nil)

type StatsSvcImpl struct {
	log       *slog.Logger
	statsRepo repository.StatsRepo
}

func NewStatsSvcImpl(log *slog.Logger, statsRepo repository.StatsRepo) *StatsSvcImpl {
	return &StatsSvcImpl{log: log, statsRepo: statsRepo}
}

func (s StatsSvcImpl) GetStatisticsByShortURL(ctx context.Context, shortURL string) (model.ShortURLStatistics, error) {
	return s.statsRepo.GetStatByShortURL(ctx, shortURL)
}

func (s StatsSvcImpl) GetStatisticsRecap(ctx context.Context) ([]model.ShortURLStatistics, error) {
	return s.statsRepo.GetStatRecap(ctx)
}
