package services

import (
	"context"
	"demo-server/internal/core/model"
	"demo-server/internal/ports"
	"demo-server/internal/ports/repository"
	"demo-server/pkg/encoder"
	"errors"
	"log/slog"
	"time"
)

var _ ports.Url = (*URLSvcImpl)(nil)

type URLSvcImpl struct {
	log       *slog.Logger
	statsRepo repository.StatsRepo
	urlRepo   repository.URLRepo
}

func (U URLSvcImpl) RequestOriginalURL(ctx context.Context, data model.RequestData) (string, error) {
	var err error

	urlData, err := U.urlRepo.GetByShortURL(ctx, data.ShortURL)
	if err != nil {
		return "", err
	}

	data.LongURL = urlData.LongURL

	if err = U.statsRepo.SaveStat(ctx, data); err != nil {
		U.log.With("err", err).With("client_data").Warn("unable to save stat")
	}

	if !urlData.ValidTill.After(time.Now()) {
		err = errors.New("ttl expired")
		return "", err
	}

	return urlData.LongURL, nil
}

func (U URLSvcImpl) GenerateShortURL(ctx context.Context, longURL string, ttl *int) (string, error) {
	var err error

	short, err := encoder.Encode(longURL)
	if err != nil {
		return "", err
	}

	var validTill time.Time

	if ttl == nil {
		validTill = time.Now().Add(time.Hour * time.Duration(24))
	}

	validTill = time.Now().Add(time.Second * time.Duration(*ttl))

	err = U.urlRepo.AddNewPair(ctx, model.URLData{
		ShortURL:  short,
		LongURL:   longURL,
		ValidTill: validTill,
		CreatedAt: time.Now(),
	})

	if err != nil {
		return "", err
	}

	return short, nil
}

func (U URLSvcImpl) DropRecordByShortURL(ctx context.Context, shortURL string) error {
	return U.urlRepo.DeleteByShortURL(ctx, shortURL)
}

func NewURLSvcImpl(log *slog.Logger, statsRepo repository.StatsRepo, urlRepo repository.URLRepo) *URLSvcImpl {
	return &URLSvcImpl{log: log, statsRepo: statsRepo, urlRepo: urlRepo}
}
